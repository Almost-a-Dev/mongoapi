const gModel = require("./guitar.model");

const createGuitar = (guitar, dueño) => {
  return new gModel({ ...guitar, dueño }).save();
}

const getGuitars = () => {
  return gModel.find({})
};

const getGuitarById = (id) => {
  return gModel.findById(id)
}

const deleteGuitarById= (id) => {
  return gModel.findByIdAndRemove(id)
}

const updateGuitarById = (id, guitar, username) => {
  // 'new' option -> this mongo return the new object to the user 
  return gModel.findOneAndUpdate({ _id: id }, { ...guitar, dueño: username }, { new: true })
};

module.exports = {
  createGuitar,
  getGuitars,
  getGuitarById,
  deleteGuitarById,
  updateGuitarById,
};