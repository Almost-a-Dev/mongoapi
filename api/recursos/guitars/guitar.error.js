class guitarDoesNotExist extends Error {
  constructor(message) {
    super(message);
    this.message = message || "Producto no existe. Operacion no puede ser completada";
    this.status = 404;
    this.name = "guitarDoesNotExist";
  }
}

class isNotTheOwner extends Error {
  constructor(message) {
    super(message);
    this.message = message || "No eres el dueño del producto. Operacion no puede ser completada";
    this.status = 401;
    this.name = "isNotTheOwner";
  }
}

module.exports = {
  guitarDoesNotExist,
  isNotTheOwner,
};
