const express = require("express");
const passport = require('passport')

const validateGuitar = require('./guitar.validate')
const log = require("./../../../utils/logger");
const gController = require('./guitar.controller')
const { errorProcessing } = require('../../libs/errorHandler');
const { guitarDoesNotExist, isNotTheOwner } = require("./guitar.error");

const jwtAuthenticate = passport.authenticate("jwt", { session: false });
const guitarRouter = express.Router();

const validarId = (req, res, next) => {
  let id = req.params.id;
  if (id.match(/^[a-fA-F0-9]{24}$/) === null) {
    res.status(400).send(`El id [${id}] suministrado en el URL no es valido`)
    return
  }
  next()
}

guitarRouter.get("/", errorProcessing((req, res) => {
  return gController.getGuitars()
    .then(guitars => {
      res.json(guitars)
    })
    /* .catch(err => {
      res.status(500).send('No se pudo leer los productos de la base de datos')
    }) */
}));

guitarRouter.post("/", [jwtAuthenticate, validateGuitar], errorProcessing((req, res) => {
  return gController.createGuitar(req.body, req.user.username)
    .then((guitar) => {
      log.info("Producto agregado a la colección productos", guitar.toObject());
      res.status(201).json(guitar);
    })
  /* let newProduct = {
    ...req.body,
    id: uuid(),
    dueño: req.user.username
  } */

    /* .catch((err) => {
      log.error("Producto no pudo ser creado", err);
      res.status(500).send("Error al tratar de crear el producto");
    }); */
  /* guitars.push(newProduct); */
}));
guitarRouter.get("/:id", validarId, errorProcessing((req, res) => {
  let id = req.params.id;
  return gController.getGuitarById(id)
    .then(guitar => {
      if (!guitar) {
        throw new guitarDoesNotExist();
        //res.status(404).send(`Producto con id ${id} no existe.`)
      } else {
        res.json(guitar)
      }
    })
    /* .catch(err => {
      log.error(`Excepcion ocurrio al tratar de obtener producto con id [${id}]`, err)
      res.status(500).send(`Error ocurrio obteniendo producto con id [${id}]`)
    }) */

  /* for (let guitar of guitars) {
    if (guitar.id === req.params.id) {
      res.json(guitar);
      return;
    }
  }
  // 404 Not found
  res.status(404).json(`El producto con id [${req.params.id}] no existe`); */
}))
guitarRouter.put("/:id", [jwtAuthenticate, validateGuitar], errorProcessing(async(req, res) => {
  let id = req.params.id;
  let usuarioAutenticado = req.user.username;
  let guitarToUpdate = await gController.getGuitarById(id);

  if (!guitarToUpdate) {
    throw new guitarDoesNotExist();
    /* res.status(404).send(`El producto con id [${id}] no existe.`)
    return */
  }

  if (guitarToUpdate.dueño !== usuarioAutenticado) {
    log.warn(`Usuario ${usuarioAutenticado} no es dueño del producto con id ${id}. Dueño real es ${guitarToUpdate.dueño}. Solicitud rechazada`);
    throw new isNotTheOwner();
    /* res.status(401).send(`No eres dueño del producto con id ${id}. Solo puedes borrar guitarras creadas por ti`);
    return; */
  }

  gController.updateGuitarById(id, req.body, usuarioAutenticado)
    .then(guitar => {
      res.json(guitar)
      log.info(`Producto con id [${id}] reemplazado con nuevo producto`, guitar.toObject())
    })
  
  /* try {
    guitarToUpdate = await gController.getGuitarById(id);
  } catch (err) {
    log.error(`Excepcion ocurrio al procesar el borrado de producto con id [${id}]`)
    res.status(500).send(`Error ocurrio borrando producto con id [${id}]`)
    return
  } */

    /* .catch(err => {
      log.error(`Excepcion al tratar de reemplazar producto con id [${id}]`, err)
      res.status(500).send(`Error ocurrio reemplazando producto con id [${id}]`)
    }) */

  /* if (!guitarToUpdate) {
    if (guitars[idx].dueño !== updatedProduct.dueño) {
      log.info(`Usuario ${req.user.dueño} no es dueño del producto con id ${updatedProduct.id}.
      Dueño real es guitars[idx].dueño. Solicitud rechazada`);
      res.status(401).send(`No eres dueño del producto con id ${updatedProduct.id}`)
      return
    }
    guitars[idx] = updatedProduct;
    res.status(200).json(updatedProduct);
    log.info(`Producto con id [${updatedProduct.id}] reemplazado con nuevo producto`, updatedProduct)
  } else {
    log.info( `Producto con id [${updatedProduct.id}] no existe. Nada que actualizar` );
    res.status(404).json(`El producto con id [${updatedProduct.id}] no existe`);
  } */
}));
guitarRouter.delete("/:id", [jwtAuthenticate, validarId], errorProcessing(async(req, res) => {
  let id = req.params.id;
  let guitarToDelete = await gController.getGuitarById(id);

  if (!guitarToDelete) {
    log.info(`Producto con id ${id} no existe`);
    throw new guitarDoesNotExist();
    /* res .status(404) .json( `El producto con id [${id}] no existe, no puede ser eliminado` );
    return; */
  }

  let usuarioAutenticado = req.user.username;
  if (guitarToDelete.dueño !== usuarioAutenticado) {
    log.info(`Usuario ${usuarioAutenticado} no es dueño del producto con id ${id}. Dueño real es ${guitarToDelete.dueño}. Solicitud rechazada`);
    throw new isNotTheOwner();
    /* res.status(401).send(`No eres dueño del producto con id ${id}. Solo puedes borrar guitarras creadas por ti`);
    return; */
  }

  let guitarDeleted = await gController.guitarToDelete(id);
  log.info(`Producto con id [${id}] fue borrado`);
  res.json(guitarDeleted);
  
  /* try {
    guitarToDelete = await gController.getGuitarById(id);
  } catch (err) {
    log.error(`Excepcion ocurrio al procesar el borrado de producto con id [${id}]`)
    res.status(500).send(`Error ocurrio borrando producto con id [${id}]`)
    return
  } */
  
  /* try {
    let guitarDeleted = await gController.guitarToDelete(id)
    log.info(`Producto con id [${id}] fue borrado`)
    res.json(guitarDeleted)
  } catch (err) {
    res.status(500).send(`Error ocurrio borrando producto con id [${id}]`)
  } */

  /* let gDeleted = guitars.splice(idx);
  log.info(`Producto con id [${req.params.id}] fue borrado`);
  res.status(200).json(gDeleted); */
}));

module.exports = guitarRouter;