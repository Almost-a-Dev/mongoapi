const mongoose = require('mongoose')

const gSchema = new mongoose.Schema({
  titulo: {
    type: String,
    required: [true, "Guitarra debe tener un titulo"],
  },
  precio: {
    type: String,
    min: 0,
    required: [true, "Guitarra debe tener un precio"],
  },
  moneda: {
    type: String,
    minlength: 3,
    maxlength: 3,
    required: [true, "Guitarra debe tener una moneda"],
  },
  dueño: {
    type: String,
    required: [true, "Guitarra debe estar asociado a un dueño"],
  },
});

module.exports = mongoose.model('guitar',gSchema);