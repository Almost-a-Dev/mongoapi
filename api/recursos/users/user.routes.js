/* const _ = require("underscore");
const uuid = require("uuid/v4");
const users = require("./../../../database").users; */


const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const validateUser = require("./user.validate")._vUser;
const validateLogin = require("./user.validate")._vLogin;
const log = require("./../../../utils/logger");
const uController = require("./user.controller");
const { errorProcessing } = require("../../libs/errorHandler");
const { DatosDeUsuarioYaEnUso, CredencialesIncorrectas } = require('./user.error')

const config = require("../../../config");
const userRouter = express.Router();

const lowerToUppercase = (req, res, next) => {
  req.body.username && (req.body.username = req.body.username.toLowerCase())
  req.body.email && (req.body.email = req.body.email.toLowerCase())
  next()
}

userRouter.get("/", errorProcessing((req, res) => {
  return uController.getUsers()
    .then((users) => {
      res.json(users);
    })
    /* .catch((err) => {
      log.error(`Error al obtener todos los susuarios`, err);
      res.status(500);
    }); */
}));
userRouter.post("/", [validateUser, lowerToUppercase], errorProcessing((req, res) => {
  let newUser = req.body;

  return uController.userExist(newUser.username, newUser.email)
    .then((userExist) => {
      if (userExist) {
        log.warn(`Email ${newUser.username} o username ${newUser.email} ya existen en la base de datos`);
        throw new DatosDeUsuarioYaEnUso();
        /* res.status(409).send("El email o usuario ingresados ya estan asociados con una cuenta");
        return; */
      }
      
      return bcrypt.hash(newUser.password, 10)
    })
    .then(hash => {
      return uController.createUser(newUser, hash).then(newUser => res.status(201).send("Usuario creado exitosamente"));
      /* if (err) {
        log.error( "Error ocurrio al tratar de obtener el hash de la contraseña", err );
        res.status(500).send("Ocurrio un error procesando creacion del usuario");
        return;
      } */
      
      /* .catch((err) => {
        log.error("Error ocurrio al tratar de crear nuevo usuario", err);
        res.status(500).send("Error ocurrio al tratar de crear nuevo usuario");
      }); */
    })
  /* .catch((err) => {
    log.error(
      `Error ocurrio al tratar de verificar si usuario [${newUser.username}] con email [${newUser.email}] ya existe`
    );
    res.status(500).send("Error ocurrio al tratar de crear nuevo usuario");
  }); */

  /* let idx = _.findIndex(users, (user) => user.username === newUser.username || user.email === newUser.email);

    if (idx !== -1) {
      log.info('El email o username ingresado ya existe en la base de datos')
      res.status(409).send('El email o username ya estan asociados a una cuenta');
      return
    }

    bcrypt.hash(newUser.password, 10, (err, hashPassword) => {
        if(err) {
            log.error('Error ocurrio al tratar de obtener el hash de la contraseña', err)
            res.status(500).send('Ocurrio un error procesando creacion del usuario')
            return
        }
        users.push({ username: newUser.username, password: hashPassword, email: newUser.email, id: uuid() });
        log.info(`Usuario ${newUser.username} agregado a la colección usuarios`);
        res.status(201).send('Usuario creado exitosamente');
    }) */
}));

userRouter.post("/login", [validateLogin, lowerToUppercase], errorProcessing(async(req, res) => {
  let noAuthUser = req.body;
  let userRegistered = await uController.getUser({ username: noAuthUser.username })

  if (!userRegistered) {
    log.info(`Usuario ${noAuthUser.username} no existe. No pudo ser autenticado`);
    throw new CredencialesIncorrectas();
    /* res.status(400).send( "Credenciales incorrectas. Asegurate que el usuario y contraseña son correctas");
    return; */
  }

  let correctPass = await bcrypt.compare(noAuthUser.password, userRegistered.password);

  if (correctPass) {
    let token = jwt.sign({ id: userRegistered.id }, config.jwt.secreto, { expiresIn: config.jwt.tiempoDeExpiracion, });
    log.info(`Usuario ${noAuthUser.username} logeado satisfactoriamente`);
    res.status(200).json({ token });
  } else {
    log.info(`Contraseña incorrecta - Usuario ${noAuthUser.username}`);
    res.status(400).send(`Credenciales incorrectas, asegurate de que el username y la contraseña son correctos`);
  }

  /* try {
    userRegistered = await uController.getUser({ username: noAuthUser.username })
  } catch (err) {
    log.error(`Error ocurrio al tratar de determinar si el usuario [${noAuthUser.username}] ya existe`, err)
    res.status(500).send('Error ocurrio durante el proceso de login.')
    return
  } */

  /* try {
    correctPass = await bcrypt.compare(noAuthUser.password, userRegistered.password)
  } catch (err) {
    log.error('Error ocurrio al tratar de verificar si la contraseña es correcta', err);
    res.status(500).send('Error ocurrio durante el proceso de login');
    return
  } */

  /* bcrypt.compare(noAuthUser.password, hashedPassword, (err, equal) => {
    if (equal) {
      let token = jwt.sign({ id: users[idx].id }, config.jwt.secreto, { expiresIn: config.jwt.tiempoDeExpiracion, });
      log.info(`Usuario ${noAuthUser.username} logeado satisfactoriamente`);
      res.status(200).json({ token });
    } else {
      log.info(`Contraseña incorrecta - Usuario ${noAuthUser.username}`);
      res.status(400).send( `Credenciales incorrectas, asegurate de que el username y la contraseña son correctos` );
    }
  }); */
}));

module.exports = userRouter;
