const uModel = require("./user.model");

const getUsers = () => {
  return uModel.find({});
};

const createUser = (user, hashPassword) => {
  return new uModel({
    ...user,
    password: hashPassword,
  }).save();
};

const userExist = (username, email) => {
  return new Promise((resolve, reject) => {
    uModel
      .find()
      .or([{ 'username': username }, { 'email': email }])
      .then((users) => {
        resolve(users.length > 0);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

const getUser = ({
  username: username,
  id: id
}) => {
  if (username) return uModel.findOne({ username: username })
  if (id) return uModel.findById(id)
  throw new Error('Funcion obtener usuario del controller fue llamada sin especificar username o id.')
}

module.exports = {
  getUsers,
  createUser,
  userExist,
  getUser,
};
