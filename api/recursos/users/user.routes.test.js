let request = require('supertest')
let bcrypt = require("bcrypt");
let jwt = require("jsonwebtoken");
let mongoose = require("mongoose");

let app = require('../../../index').app
let server = require('../../../index').server
let User = require('./user.model')
let config = require("../../../config");

let dummyUsuarios = [
  {
    username: "daniel",
    email: "daniel@gmail.com",
    password: "holaquetal",
  },
  {
    username: "ricardo",
    email: "ricardo@gmail.com",
    password: "quepaso",
  },
  {
    username: "diego",
    email: "diego@gmail.com",
    password: "nomedigas",
  },
];

function usuarioExisteYAtributosSonCorrectos(user, done) {
  User.find({ username: user.username })
    .then((users) => {
      expect(users).toBeInstanceOf(Array);
      expect(users).toHaveLength(1);
      expect(users[0].username).toEqual(user.username);
      expect(users[0].email).toEqual(user.email);

      let equal = bcrypt.compareSync(user.password, users[0].password);
      expect(equal).toBeTruthy();
      done();
    })
    .catch((err) => {
      done(err);
    });
}

async function usuarioNoExiste(user, done) {
  try {
    let users = await User.find().or([
      { username: user.username },
      { email: user.email },
    ]);
    expect(users).toHaveLength(0);
    done();
  } catch (err) {
    done(err);
  }
}

describe('Usuarios', () => {

    beforeEach((done) => {
        User.deleteMany({}, (err) => done());
    })

    afterAll(() => {
        server.close()
    })

    describe("GET /users", () => {
      test("Si no hay usuarios, debería retornar un array vació", (done) => {
        request(app)
          .get("/users")
          .end((err, res) => {
            expect(res.status).toBe(200);
            expect(res.body).toBeInstanceOf(Array);
            expect(res.body).toHaveLength(0);
            done();
          });
      });

      test("Si existen usuarios, debería retornarlos en un array", (done) => {
        Promise.all(dummyUsuarios.map((user) => new User(user).save())).then(
          (users) => {
            request(app)
              .get("/users")
              .end((err, res) => {
                expect(res.status).toBe(200);
                expect(res.body).toBeInstanceOf(Array);
                expect(res.body).toHaveLength(3);
                done();
              });
          }
        );
      });
    });

    describe("POST /users", () => {
      test("Un usuario que cumple las condiciones debería ser creado", (done) => {
        request(app)
          .post("/users")
          .send(dummyUsuarios[0])
          .end((err, res) => {
            expect(res.status).toBe(201);
            expect(typeof res.text).toBe("string");
            expect(res.text).toEqual("Usuario creado exitósamente.");
            usuarioExisteYAtributosSonCorrectos(dummyUsuarios[0], done);
          });
      });

      test("Crear un usuario con un username ya registrado debería fallar", (done) => {
        Promise.all(
          dummyUsuarios.map((user) => new User(user).save())
        ).then((users) => {
          request(app)
            .post("/users")
            .send({
              username: "daniel",
              email: "danielnuevoemail@gmail.com",
              password: "cuidadoarriba",
            })
            .end((err, res) => {
              expect(res.status).toBe(409);
              expect(typeof res.text).toBe("string");
              done();
            });
        });
      });

      test("Crear un usuario con un email ya registrado debería fallar", (done) => {
        Promise.all(dummyUsuarios.map((user) => new User(user).save())).then(
          (usuarios) => {
            request(app)
              .post("/users")
              .send({
                username: "nuevodaniel",
                email: "daniel@gmail.com",
                password: "cuidadoarriba",
              })
              .end((err, res) => {
                expect(res.status).toBe(409);
                expect(typeof res.text).toBe("string");
                done();
              });
          }
        );
      });

      test("Un usuario sin username no debería ser creado", (done) => {
        request(app)
          .post("/users")
          .send({
            email: "daniel@gmail.com",
            password: "contraseña",
          })
          .end((err, res) => {
            expect(res.status).toBe(400);
            expect(typeof res.text).toBe("string");
            done();
          });
      });

      test("Un usuario sin contraseña no debería ser creado", (done) => {
        request(app)
          .post("/users")
          .send({
            username: "daniel",
            email: "daniel@gmail.com",
          })
          .end((err, res) => {
            expect(res.status).toBe(400);
            expect(typeof res.text).toBe("string");
            done();
          });
      });

      test("Un usuario sin email no debería ser creado", (done) => {
        request(app)
          .post("/users")
          .send({
            username: "daniel",
            password: "contraseña",
          })
          .end((err, res) => {
            expect(res.status).toBe(400);
            expect(typeof res.text).toBe("string");
            done();
          });
      });

      test("Un usuario con un email inválido no debería ser creado", (done) => {
        let usuario = {
          username: "daniel",
          email: "@gmail.com",
          password: "contraseña",
        };
        request(app)
          .post("/users")
          .send(usuario)
          .end((err, res) => {
            expect(res.status).toBe(400);
            expect(typeof res.text).toBe("string");
            usuarioNoExiste(usuario, done);
          });
      });

      test("Un usuario con un username con menos de 3 caracteres no debería ser creado", (done) => {
        let user = {
          username: "da",
          email: "daniel@gmail.com",
          password: "contraseña",
        };
        request(app)
          .post("/users")
          .send(user)
          .end((err, res) => {
            expect(res.status).toBe(400);
            expect(typeof res.text).toBe("string");
            usuarioNoExiste(user, done);
          });
      });

      test("Un usuario con un username con más de 30 caracteres no debería ser creado", (done) => {
        let user = {
          username: "daniel".repeat(10),
          email: "daniel@gmail.com",
          password: "contraseña",
        };
        request(app)
          .post("/users")
          .send(user)
          .end((err, res) => {
            expect(res.status).toBe(400);
            expect(typeof res.text).toBe("string");
            usuarioNoExiste(user, done);
          });
      });

      test("Un usuario cuya contraseña tenga menos de 6 caracteres no debería ser creado", (done) => {
        let user = {
          username: "daniel",
          email: "daniel@gmail.com",
          password: "abc",
        };
        request(app)
          .post("/users")
          .send(user)
          .end((err, res) => {
            expect(res.status).toBe(400);
            expect(typeof res.text).toBe("string");
            usuarioNoExiste(user, done);
          });
      });

      test("Un usuario cuya contraseña tenga más de 200 caracteres no debería ser creado", (done) => {
        let user = {
          username: "daniel",
          email: "daniel@gmail.com",
          password: "contraseña".repeat(40),
        };
        request(app)
          .post("/users")
          .send(user)
          .end((err, res) => {
            expect(res.status).toBe(400);
            expect(typeof res.text).toBe("string");
            usuarioNoExiste(user, done);
          });
      });

      test("El username y email de un usuario válido deben ser guardados en lowercase", (done) => {
        let user = {
          username: "DaNIEL",
          email: "APPdelante@GMAIL.com",
          password: "pruebapruebaprueba",
        };
        request(app)
          .post("/users")
          .send(user)
          .end((err, res) => {
            expect(res.status).toBe(201);
            expect(typeof res.text).toBe("string");
            expect(res.text).toEqual("Usuario creado exitósamente.");
            usuarioExisteYAtributosSonCorrectos(
              {
                username: user.username.toLowerCase(),
                email: user.email.toLowerCase(),
                password: user.password,
              },
              done
            );
          });
      });
    });

    describe("POST /login", () => {
      test("Login debería fallar para un request que no tiene username", (done) => {
        let bodyLogin = {
          password: "holaholahola",
        };
        request(app)
          .post("/users/login")
          .send(bodyLogin)
          .end((err, res) => {
            expect(res.status).toBe(400);
            expect(typeof res.text).toBe("string");
            done();
          });
      });

      test("Login debería fallar para un request que no tiene password", (done) => {
        let bodyLogin = {
          username: "noexisto",
        };
        request(app)
          .post("/users/login")
          .send(bodyLogin)
          .end((err, res) => {
            expect(res.status).toBe(400);
            expect(typeof res.text).toBe("string");
            done();
          });
      });

      test("Login debería fallar para un usuario que no esta registrado", (done) => {
        let bodyLogin = {
          username: "noexisto",
          password: "holaholahola",
        };
        request(app)
          .post("/users/login")
          .send(bodyLogin)
          .end((err, res) => {
            expect(res.status).toBe(400);
            expect(typeof res.text).toBe("string");
            done();
          });
      });

      test("Login debería fallar para un usuario registrado que suministra una contraseña incorrecta", (done) => {
        let user = {
          username: "daniel",
          email: "daniel@gmail.com",
          password: "perrosamarillos",
        };

        new User({
          username: user.username,
          email: user.email,
          password: bcrypt.hashSync(user.password, 10),
        })
          .save()
          .then((newUser) => {
            request(app)
              .post("/users/login")
              .send({
                username: user.username,
                password: "arrozverde",
              })
              .end((err, res) => {
                expect(res.status).toBe(400);
                expect(typeof res.text).toBe("string");
                done();
              });
          })
          .catch((err) => {
            done(err);
          });
      });

      test("Usuario registrado debería obtener un JWT token al hacer login con credenciales correctas", (done) => {
        let user = {
          username: "daniel",
          email: "daniel@gmail.com",
          password: "perrosamarillos",
        };

        new User({
          username: user.username,
          email: user.email,
          password: bcrypt.hashSync(user.password, 10),
        })
          .save()
          .then((newUser) => {
            request(app)
              .post("/users/login")
              .send({
                username: user.username,
                password: user.password,
              })
              .end((err, res) => {
                expect(res.status).toBe(200);
                expect(res.body.token).toEqual(
                  jwt.sign({ id: newUser._id }, config.jwt.secreto, {
                    expiresIn: config.jwt.tiempoDeExpiración,
                  })
                );
                done();
              });
          })
          .catch((err) => {
            done(err);
          });
      });

      test("Al hacer login no debe importar la capitalización del username", (done) => {
        let user = {
          username: "daniel",
          email: "daniel@gmail.com",
          password: "perrosamarillos",
        };

        new User({
          username: user.username,
          email: user.email,
          password: bcrypt.hashSync(user.password, 10),
        })
          .save()
          .then((newUser) => {
            request(app)
              .post("/users/login")
              .send({
                username: "DaNIEL",
                password: user.password,
              })
              .end((err, res) => {
                expect(res.status).toBe(200);
                expect(res.body.token).toEqual(
                  jwt.sign({ id: newUser._id }, config.jwt.secreto, {
                    expiresIn: config.jwt.tiempoDeExpiración,
                  })
                );
                done();
              });
          })
          .catch((err) => {
            done(err);
          });
      });
    });
})