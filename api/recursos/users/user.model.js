const mongoose = require("mongoose");

const uSchema = new mongoose.Schema({
  username: {
    type: String,
    minlength: 1,
    required: [true, "Usuario debe tener username"],
  },
  password: {
    type: String,
    minlength: 1,
    required: [true, "Usuario debe tener password"],
  },
  email: {
    type: String,
    minlength: 1,
    required: [true, "Usuario debe tener email"],
  },
});

module.exports = mongoose.model("user", uSchema);
