class DatosDeUsuariosYaEnUso extends Error {
  constructor(message) {
    super(message);
    this.message =
      message || "El email o usuario ya stan asociados con una cuenta.";
    this.status = 490;
    this.name = "DatosDeUsuariosYaEnUso";
  }
}

class CredencialesIncorrectas extends Error {
  constructor(message) {
    super(message);
    this.message = message || "Credenciales incorrectas. Asegurate que el usuario y contraseña son correctas";
    this.status = 400;
    this.name = "CredencialesIncorrectas";
  }
}

module.exports = {
  DatosDeUsuariosYaEnUso,
  CredencialesIncorrectas,
};