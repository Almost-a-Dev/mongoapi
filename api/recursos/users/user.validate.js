const Joi = require("joi");
const log = require("./../../../utils/logger");

const userBluePrint = Joi.object().keys({
  username: Joi.string().alphanum().min(3).max(30).required(),
  password: Joi.string().min(6).max(200).required(),
  email: Joi.string().email().required()
});

const loginBluePrint = Joi.object().keys({
  username: Joi.string().required(),
  password: Joi.string().required(),
});

const _vUser = (req, res, next) => {
  const response = Joi.validate(req.body, userBluePrint, { abortEarly: false, convert: false, });
  if (response.error === null) {
    next();
  } else {
    //console.log(response.error.details);
    //let errDetails = response.error.details.reduce( (acc, err) => acc + `[${err.message}]`, "" );
    log.info( "Usuario fallo validacion: ", response.error.details.map(err => err.message) );
    res .status(400) .json( `Tu usuario no cumple con la estructura requerida` );
  }
};

const _vLogin = (req, res, next) => {
  const response = Joi.validate(req.body, loginBluePrint, { abortEarly: false, convert: false, });
  if (response.error === null) {
    next();
  } else {
    log.info( "Login de usuario fallido: ", response.error.details.map((err) => err.message) );
    res.status(400).json(`Login fallido, ingresa las credenciales correctas`);
  }
};

module.exports = {
  _vUser,
  _vLogin
}