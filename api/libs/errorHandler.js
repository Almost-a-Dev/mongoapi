const log = require("../../utils/logger");
const mongoose = require("mongoose");

const errorProcessing = (fn) => (req, res, next) => fn(req, res, next).catch((next));

const processingErrorDB = (err, req, res, next) => {
  if (err instanceof mongoose.Error || err.name === "MongoError") {
    log.error(`Ocurrio un error relacionado a mongoose`, err);
    err.message = "Error relacionado a la base de datos ocurrio inesperadamente. Para ayuda contacte a alejandro@mail.com";
    err.status = 500;
  }
  next(err);
};

const errorProduction = (err, req, res, next) => {
  res.status(err.status || 500);
  res.send({
    message: err.message,
  });
};

const errorDevelopment = (err, req, res, next) => {
  res.status(err.status || 500);
  res.send({
    message: err.message,
    stack: err.stack || "",
  });
};

module.exports = {
  errorProcessing,
  processingErrorDB,
  errorDevelopment,
  errorProduction,
};
