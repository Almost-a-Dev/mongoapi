/* const _ = require('underscore'); */
const log = require("./../../utils/logger");
/* const users = require('../../database').users; */
/* const bcrypt = require('bcrypt'); */
const passportJWT = require('passport-jwt');
const config = require('../../config');

const uController = require('../recursos/users/user.controller')

let jwtOptions = {
  secretOrKey: config.jwt.secreto,
  jwtFromRequest: passportJWT.ExtractJwt.fromAuthHeaderAsBearerToken(),
};

module.exports = new passportJWT.Strategy(jwtOptions, (jwtPayload, next) => {

  uController.getUser({ id: jwtPayload.id })
    .then(user => {
      if (!user) {
        log.info(`JWT token no es valido. Usuario con id ${jwtPayload.id} no existe.`)
        next(null, false)
        return
      }
      log.info(`Token valido - ${user.username}.`);
      next(null, {
        username: user.username,
        id: user.id,
      });
    })
    .catch(err => {
      log.info('Error ocurrio al tratar de validar un token', err);
      next(err);
    })

/*   let idx = _.findIndex(users, (user) => user.id === jwtPayload.id);

  if (idx === -1) {
    log.info(`JWT token no es valido. Usuario con id ${jwtPayload.id} no existe.`)
    next(null, false)
  } else {
    log.info(`Token valido - ${users[idx].username}.`);
    next(null, {
      username: users[idx].username,
      id: users[idx].id,
    });
  } */
});


// Old basic stratgey - passport middleware definition
/* module.exports = (username, password, next) => {
  let idx = _.findIndex(users, user => user.username === username);

  if (idx === -1) {
    log.info(`Usuario ${username} no existe en la base de datos`)
    next(null, false)
    return
  }

  let hashedPassword = users[idx].password;
  bcrypt.compare(password, hashedPassword, (err, equal) => {
    if (equal) {
      log.info(`Usuario ${username} logeado satisfactoriamente`);
      next(null, true);
    } else {
      log.info(`Contraseña incorrecta - Usuario ${username}`);
      next(null, false);
    }
  });
}; */