/* modules... */
const express = require("express");
const bodyParser = require("body-parser");
const morgan = require('morgan');
const mongoose = require('mongoose')

/* own files... */
const guitarRouter = require("./api/recursos/guitars/guitar.routes");
const userRouter = require("./api/recursos/users/user.routes");
const logger = require('./utils/logger');
const authJWT = require('./api/libs/auth')
const config = require('./config')
const errorHandler = require('./api/libs/errorHandler')

const passport = require("passport");
passport.use(authJWT);

/* old configurations...
/* const basicStrategy = require("passport-http").BasicStrategy; */
/* const auth = require('./api/libs/auth') */
/* passport.use(new basicStrategy(auth)); */

mongoose.connect('mongodb://127.0.0.1:27017/vendeguitarras', { useNewUrlParser: true, useUnifiedTopology: true })
mongoose.connection.on('error', () => {
  logger.error('Fallo la conexion con MongoDB')
  process.exit(1)
})

//ps aux | grep mongo

//Middlewares
const app = express();
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(morgan('short', {
    stream: {
        write: message => logger.info(message)
    }
}));

//Router modules
app.use("/guitars", guitarRouter);
app.use("/users", userRouter);

/* errorHandler Middleware */
app.use(errorHandler.processingErrorDB);
if (config.ambiente === 'prod') {
  app.use(errorHandler.errorProduction)
} else {
  app.use(errorHandler.errorDevelopment);
}

//api test
/* app.get("/", passport.authenticate('jwt', { session: false }), (req, res) => {
  logger.info(req.user)
  res.json("API de ENC");
}); */

const server = app.listen(config.port, () => {
  logger.info("Escuchando en el puerto 3000");
});

module.exports = {
  app, server
}