const winston = require("winston");

const addTimestamp = winston.format((info) => {
  info.message = `${new Date().toISOString()} ${info.message}`;
  return info;
});

const logger = winston.createLogger({
  transports: [
    new winston.transports.Console({
      level: "debug",
      json: false,
      handleExceptions: true,
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.simple()
      ),
      prettyPrint: (object) => {
        return JSON.stringify(object);
      },
    }),
    new winston.transports.File({
      level: "info",
      json: false,
      handleExceptions: true,
      format: winston.format.combine(addTimestamp(), winston.format.simple()),
      maxsize: 100, //5MB
      maxFiles: 5,
      filename: `${__dirname}/../logs/logs-application.log`,
      prettyPrint: (object) => {
        return JSON.stringify(object);
      },
    }),
  ],
});

module.exports = logger;